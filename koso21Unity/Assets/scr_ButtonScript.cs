using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_ButtonScript : MonoBehaviour
{
    public int buttonID;
    private scr_InputManager ref_InputManager;
    private scr_FlavorSpawner ref_FlavorSpawner;

    private void Start()
    {
        ref_InputManager = GetComponentInParent<scr_InputManager>();
    }

    public void AssignFlavor()
    {
        if (ref_FlavorSpawner == null)
            ref_FlavorSpawner = GetComponentInParent<scr_FlavorSpawner>();
        ref_FlavorSpawner.AssignFlavor(transform.position, GetComponent<RectTransform>().rect, buttonID);
    }

    public void AssignCupSize()
    {
        if (ref_FlavorSpawner == null)
            ref_FlavorSpawner = GetComponentInParent<scr_FlavorSpawner>();
        ref_FlavorSpawner.AssignCupSize(transform.position, GetComponent<RectTransform>().rect, buttonID);
    }

    public void AssignAsSpriteTarget()
    {
        GetComponentInParent<scr_IceCreamBuilder>().ReceiveCoordinates(transform, GetComponent<RectTransform>());
    }

    public void PointerUp()
    {
        ref_InputManager.ReadInput(buttonID);
    }
}
