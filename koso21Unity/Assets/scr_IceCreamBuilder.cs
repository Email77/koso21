using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class scr_IceCreamBuilder : MonoBehaviour
{
    public GameObject defaultVisualObject;
    public GameObject _parent;
    private GameObject visualIceCream;
    private GameObject visualBase;
    private Vector2 size;
    private Vector3 originalPosition;

    public void ReceiveCoordinates(Transform received, RectTransform receivedRect)
    {
        visualBase = Instantiate(defaultVisualObject, _parent.transform);
        originalPosition = received.position;
        visualIceCream = Instantiate(defaultVisualObject, _parent.transform);
        size = receivedRect.sizeDelta;
        ChangeVisuals();
    }

    public void ChangeVisuals()
    {
        int[] storage = new int[2];
        storage[0] = GetComponent<scr_CurrentSelectionStorage>().current.currentSize;
        storage[1] = GetComponent<scr_CurrentSelectionStorage>().current.currentFlavor;

        if (storage[0] == 0)
            ChangeOpacity(0f, 0f);
        else
        {
            if (storage[1] != 0)
            {
                ChangeOpacity(1f, 1f);
                visualIceCream.GetComponent<Image>().sprite = GetComponent<scr_FlavorSpawner>().ReturnFlavorSprite(storage[1]);
                CorrectAspectRatio(GetComponent<scr_FlavorSpawner>().ReturnFlavorSprite(storage[1]), true);
            }
            else
                ChangeOpacity(1f, 0f);
            visualBase.GetComponent<Image>().sprite = GetComponent<scr_FlavorSpawner>().ReturnSizeSprite(storage[0]);
            CorrectAspectRatio(GetComponent<scr_FlavorSpawner>().ReturnSizeSprite(storage[0]), false);
        }
    }

    private void CorrectAspectRatio(Sprite receivedSprite, bool baseFalseFlavorTrue)
    {
        float ratio = receivedSprite.rect.height / receivedSprite.rect.width;
        if (baseFalseFlavorTrue)
        {
            visualIceCream.GetComponent<RectTransform>().sizeDelta = new Vector2(size.y, size.y * ratio);
            visualIceCream.transform.position = originalPosition + new Vector3(0f, size.y * 0.55f, 0f);
        }
        else
        {
            visualBase.GetComponent<RectTransform>().sizeDelta = new Vector2(size.y, size.y * ratio);
            visualBase.transform.position = originalPosition;
        }
    }

    private void ChangeOpacity(float alphaOne, float alphaTwo)
    {
        Color defaulColor = visualIceCream.GetComponent<Image>().color;
        visualIceCream.GetComponent<Image>().color = new Color(defaulColor.r, defaulColor.g, defaulColor.b, alphaTwo);
        visualBase.GetComponent<Image>().color = new Color(defaulColor.r, defaulColor.g, defaulColor.b, alphaOne);
    }
}
