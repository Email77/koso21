using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class scr_CustomerInputs : MonoBehaviour
{
    public GameObject image;
    public int customerOrderSize;
    public int customerOrderFlavor;

    private scr_FlavorSpawner ref_FlavorSpawner;
    private Rect refRect;
    private GameObject sizeImage;
    private GameObject flavorImage;
    private Canvas ref_canvas;
    void Start()
    {
        refRect = GetComponent<RectTransform>().rect;
        ref_FlavorSpawner = GetComponentInParent<scr_FlavorSpawner>();
        ref_canvas = GetComponentInParent<Canvas>();
        customerOrderSize = ref_FlavorSpawner.cupSizes[Random.Range(0, ref_FlavorSpawner.cupSizes.Length)].flavourID;
        customerOrderFlavor = ref_FlavorSpawner.flavors[Random.Range(0, ref_FlavorSpawner.flavors.Length)].flavourID;
    }

    public void spawnOrder()
    {
        sizeImage = Instantiate(image, transform);
        flavorImage = Instantiate(image, transform);
        float imageWidth = refRect.width / 2;
        Sprite sizeSprite = ref_FlavorSpawner.ReturnSizeSprite(customerOrderSize);
        Sprite flavorSprite = ref_FlavorSpawner.ReturnFlavorSprite(customerOrderFlavor);
        float sizeRatio = sizeSprite.rect.height / sizeSprite.rect.width;
        float flavorRatio = flavorSprite.rect.height / flavorSprite.rect.width;
        sizeImage.GetComponent<RectTransform>().sizeDelta = new Vector2(imageWidth, imageWidth * sizeRatio);
        flavorImage.GetComponent<RectTransform>().sizeDelta = new Vector2(imageWidth, imageWidth * flavorRatio);
        sizeImage.transform.position = transform.position;
        flavorImage.transform.position = transform.position;

        if (transform.position.x <= ref_canvas.GetComponent<RectTransform>().rect.width / 2)
        {
            sizeImage.transform.position += new Vector3(-imageWidth / 2, refRect.height / 2, 0f);
            flavorImage.transform.position += new Vector3(-imageWidth / 2, refRect.height / 2 + (imageWidth * sizeRatio * 0.45f), 0f);
        }
        else
        {
            sizeImage.transform.position += new Vector3(refRect.width / 2 - imageWidth / 2, refRect.height / 2, 0f);
            flavorImage.transform.position += new Vector3(refRect.width / 2 - imageWidth / 2, refRect.height / 2 + (imageWidth * sizeRatio * 0.45f), 0f);
        }
        sizeImage.GetComponent<Image>().sprite = sizeSprite;
        flavorImage.GetComponent<Image>().sprite = flavorSprite;
    }

    public IEnumerator GoToTarget(float targetPosition, float originPos)
    {
        float targetTime = 1.5f;
        for (float i = 0; i <= targetTime; i += Time.deltaTime)
        {
            yield return null;
            transform.position = new Vector3(Mathf.Lerp(originPos, targetPosition, i / targetTime), transform.position.y, transform.position.z);
        }
        yield return null;
        if (transform.position.x >= ref_canvas.GetComponent<RectTransform>().rect.width || transform.position.x <= 0f)
        {
            yield return null;
            Delete();
        }
        else
        {
            yield return null;
            GetComponent<scr_CustomerInputs>().spawnOrder();
        }
    }

    public void GoAway()
    {
        float targetX;
        if (transform.position.x <= ref_canvas.GetComponent<RectTransform>().rect.width / 2)
            targetX = -refRect.width / 2;
        else
            targetX = ref_canvas.GetComponent<RectTransform>().rect.width + (refRect.width / 2);
        StartCoroutine(GoToTarget(targetX, transform.position.x));
    }

    private void Delete()
    {
        Destroy(gameObject);
        GetComponentInParent<scr_CustomerHandler>().RemoveFromList(gameObject,(gameObject.transform.position.x <= ref_canvas.GetComponent<RectTransform>().rect.width / 2));
    }
}
