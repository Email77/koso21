using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class scr_FlavorSpawner : MonoBehaviour
{
    public GameObject spriteGameobject;
    public GameObject _parent;
    public Sprite[] flavorSprites;
    public Flavor[] flavors;
    public Sprite[] cupSizeSprites;
    public Flavor[] cupSizes;

    private int assignedFlavors = 0;
    private int assignedCupSizes = 0;
    private bool usedOnce;

    private void Start()
    {
        MakeFlavours();
        MakeCupSizes();
    }

    private void MakeFlavours()
    {
        for (int i = 0; i < flavors.Length; i++)
        {
            flavors[i]._sprite = flavorSprites[i];
            flavors[i].flavourID = i + 1;
        }
    }

    private void MakeCupSizes()
    {
        for (int i = 0; i < cupSizes.Length; i++)
        {
            cupSizes[i]._sprite = cupSizeSprites[i];
            cupSizes[i].flavourID = i + 1;
        }
    }

    public void AssignCupSize(Vector3 rec_position, Rect _rect, int buttonID)
    {
        if (!usedOnce)
            MakeCupSizes();

        GameObject newSprite = Instantiate(spriteGameobject, _parent.transform);
        float ratio = cupSizes[assignedCupSizes]._sprite.rect.height / cupSizes[assignedCupSizes]._sprite.rect.width;
        newSprite.GetComponent<RectTransform>().sizeDelta = new Vector2 (_rect.height, _rect.height * ratio);
        newSprite.transform.position = rec_position + new Vector3(0, 0, -5);
        newSprite.GetComponent<Image>().sprite = cupSizes[assignedCupSizes]._sprite;
        cupSizes[assignedCupSizes].assignedButtonID = buttonID;
        assignedCupSizes++;
    }

    public void AssignFlavor(Vector3 rec_position, Rect _rect, int buttonID)
    {
        if (!usedOnce)
            MakeFlavours();

        GameObject newSprite = Instantiate(spriteGameobject, _parent.transform);
        float ratio = flavors[assignedFlavors]._sprite.rect.height / flavors[assignedFlavors]._sprite.rect.width;
        newSprite.GetComponent<RectTransform>().sizeDelta = new Vector2(_rect.height, _rect.height * ratio);
        newSprite.transform.position = rec_position + new Vector3(0, 0, -5);
        newSprite.GetComponent<Image>().sprite = flavors[assignedFlavors]._sprite;
        flavors[assignedFlavors].assignedButtonID = buttonID;
        assignedFlavors++;
    }

    public int ReturnFlavor(int receivedButtonID)
    {
        for (int i = 0; i < flavors.Length; i++)
        {
            if (flavors[i].assignedButtonID == receivedButtonID)
                return flavors[i].flavourID;
        }
        return 0;
    }

    public int ReturnSize(int receivedButtonID)
    {
        for (int i = 0; i < cupSizes.Length; i++)
        {
            if (cupSizes[i].assignedButtonID == receivedButtonID)
                return cupSizes[i].flavourID;
        }
        return 0;
    }

    public Sprite ReturnSizeSprite(int receivedSizeID)
    {
        return cupSizes[receivedSizeID - 1]._sprite;
    }
    public Sprite ReturnFlavorSprite(int receivedFlavorID)
    {
        return flavors[receivedFlavorID - 1]._sprite;
    }
}

[System.Serializable]
public class Flavor
{
    public Sprite _sprite;
    public int flavourID;
    public int assignedButtonID;
}