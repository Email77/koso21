using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class scr_CustomerHandler : MonoBehaviour
{
    public List<Customer> customers;
    public Sprite[] customerSprites;
    public GameObject spriteObject;
    public GameObject spriteParent;

    private float height;
    private float reftransformleft;
    private float reftransformright;
    private Vector3 targetleft;
    private Vector3 targetright;

    public void ReceiveCustomerCoordinates(Vector3 receivedTransform, float buttonWidth, float buttonHeight)
    {
        Customer last = new Customer();
        GameObject newSprite = Instantiate(spriteObject, spriteParent.transform);
        height = buttonHeight;
        last.ReceiveSprite(newSprite, customerSprites[Random.Range(0, customerSprites.Length)], receivedTransform, buttonHeight);
        if (receivedTransform.x <= GetComponentInParent<Canvas>().GetComponent<RectTransform>().rect.width / 2)
        {
            reftransformleft = last.spriteObject.GetComponentInParent<Canvas>().GetComponent<RectTransform>().rect.width + last.spriteObject.GetComponent<Image>().sprite.rect.width / 2;
            targetleft = receivedTransform;
        }
        else
        {
            reftransformright = 0f - last.spriteObject.GetComponent<Image>().sprite.rect.width / 2;
            targetright = receivedTransform;
        }
        StartCoroutine(last.spriteObject.GetComponent<scr_CustomerInputs>().GoToTarget(last.targetPosition.x, receivedTransform.x));
        customers.Add(last);
    }

    public void RemoveFromList(GameObject deleteme, bool left)
    {
        foreach(Customer x in customers)
        {
            if (x.spriteObject == deleteme)
            {
                customers.Remove(x);
                Customer last = new Customer();
                GameObject newSprite = Instantiate(spriteObject, spriteParent.transform);
                if (left)
                {
                    last.ReceiveSprite(newSprite, customerSprites[Random.Range(0, customerSprites.Length)], targetleft, height);
                    StartCoroutine(last.spriteObject.GetComponent<scr_CustomerInputs>().GoToTarget(targetleft.x, reftransformright));
                }
                else
                {
                    last.ReceiveSprite(newSprite, customerSprites[Random.Range(0, customerSprites.Length)], targetright, height);
                    StartCoroutine(last.spriteObject.GetComponent<scr_CustomerInputs>().GoToTarget(targetright.x, reftransformleft));
                }
                customers.Add(last);
            }
        }
    }
}

[System.Serializable]
public class Customer
{
    public GameObject spriteObject;
    public Vector3 targetPosition;
    public float originPos;

    public void ReceiveSprite(GameObject received, Sprite receivedSprite, Vector3 receivedTarget, float recHeight)
    {
        targetPosition = receivedTarget;
        spriteObject = received;
        spriteObject.GetComponent<Image>().sprite = receivedSprite;

        float ratio = receivedSprite.rect.height / receivedSprite.rect.width;
        spriteObject.GetComponent<RectTransform>().sizeDelta = new Vector2(recHeight, recHeight * ratio);

        if (targetPosition.x >= spriteObject.GetComponentInParent<Canvas>().GetComponent<RectTransform>().rect.width / 2)
            originPos = spriteObject.GetComponentInParent<Canvas>().GetComponent<RectTransform>().rect.width + receivedSprite.rect.width / 2;
        else
            originPos = 0f - receivedSprite.rect.width / 2;

        spriteObject.transform.position = new Vector3(originPos, targetPosition.y, targetPosition.z);
    }
}