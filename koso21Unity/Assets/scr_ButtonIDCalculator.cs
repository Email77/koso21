using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_ButtonIDCalculator : MonoBehaviour
{
    public int currentID;
    public void ResetValues()
    {
        currentID = 0;
    }

    public void UpdateValues()
    {
        currentID++;
    }
}
