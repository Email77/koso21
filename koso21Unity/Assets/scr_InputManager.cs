using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_InputManager : MonoBehaviour
{
    public void ReadInput(int receivedInput)
    {
        switch (receivedInput)
        {
            default:
                int placeholderID = 0;
                placeholderID = GetComponentInParent<scr_FlavorSpawner>().ReturnFlavor(receivedInput);
                if (placeholderID != 0)
                    if (GetComponentInParent<scr_CurrentSelectionStorage>().current.currentSize != 0)
                        GetComponentInParent<scr_CurrentSelectionStorage>().ChangeFlavor(placeholderID);
                placeholderID = GetComponentInParent<scr_FlavorSpawner>().ReturnSize(receivedInput);
                if (placeholderID != 0)
                {
                    GetComponentInParent<scr_CurrentSelectionStorage>().ChangeSize(placeholderID);
                    GetComponentInParent<scr_CurrentSelectionStorage>().ChangeFlavor(0);
                }
                GetComponentInParent<scr_IceCreamBuilder>().ChangeVisuals();
                break;
                
        }
    }
}
