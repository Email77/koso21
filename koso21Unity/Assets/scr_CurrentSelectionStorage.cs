using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_CurrentSelectionStorage : MonoBehaviour
{
    public CurrentSelection current;

    public void ResetValues()
    {
        current = new CurrentSelection();
    }

    public void ChangeFlavor(int flavorID)
    {
        current.currentFlavor = flavorID;
    }

    public void ChangeSize(int sizeID)
    {
        current.currentSize = sizeID;
    }

    public CurrentSelection sendCurrentSelection()
    {
        return current;
    }
}

[System.Serializable]
public struct CurrentSelection
{
    public int currentFlavor;
    public int currentSize;
}
