using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class scr_MenuSystem : MonoBehaviour
{
    #region variables
    public GameObject backgroundObject;
    public GameObject buttonObject;


    public Sprite defaultBackground;
    public Sprite defaultButtonBackground;
    public float percentageOfScreenUsageOverall;
    public int[] sectionsPerLine;
    public float[] heightPercentagePerSection;
    public MenuSection[] menuSectors;


    private RectTransform myCanvasRect;
    private float screenHeight;
    private float screenWidth;
    #endregion

    public void Start()
    {
        UpdateScreen();
    }

    public void UpdateScreen()
    {
        GetComponent<scr_ButtonIDCalculator>().ResetValues();
        myCanvasRect = GetComponentInParent<Canvas>().GetComponent<RectTransform>();
        CreateSections();
    }

    private void CreateSections()
    {
        screenHeight = myCanvasRect.rect.height;
        screenWidth = myCanvasRect.rect.width;
        float usedHeight = screenHeight;
        int currentSector = 0;

        for (int i = 0; i < sectionsPerLine.Length; i++)
        {
            float tempHeight = 0f;
            for (int b = 0; b < sectionsPerLine[i]; b++)
            {
                float height = screenHeight * percentageOfScreenUsageOverall * heightPercentagePerSection[i];
                float width = screenWidth * percentageOfScreenUsageOverall / sectionsPerLine[i];
                float locHeight = usedHeight - ((screenHeight - screenHeight * percentageOfScreenUsageOverall) / 2) - height / 2;
                float locWidth = screenWidth - width * b - width / 2 - ((screenWidth - screenWidth * percentageOfScreenUsageOverall) / 2);

                menuSectors[currentSector].ReceiveInfo(locHeight, locWidth, height, width, gameObject, backgroundObject, defaultBackground, defaultButtonBackground, buttonObject);

                tempHeight = height;
                currentSector++;
            }
            usedHeight -= tempHeight;
        }
    }

    [System.Serializable]
    public class MenuSection
    {
        #region variables
        public Sprite customBackground;
        public float percentageOfSectorUsageOverall;
        public float buttonWidthScale;
        public ButtonSection[] buttons;

        private float locHeight, locWidth, height, width;
        private GameObject parent, BGObject, buttonObject;
        private Sprite defaultBG, defaultButtonBG;
        #endregion

        public void ReceiveInfo(float recLocHeight, float recLocWidth, float recHeight, float recWidth, GameObject recParent, GameObject recBGObject, Sprite recDefaultBG, Sprite recDefaultButtonBG, GameObject recButtonObject)
        {
            locHeight = recLocHeight;
            locWidth = recLocWidth;
            height = recHeight;
            width = recWidth;
            parent = recParent;
            BGObject = recBGObject;
            buttonObject = recButtonObject;
            defaultBG = recDefaultBG;
            defaultButtonBG = recDefaultButtonBG;

            CreateSection();
        }

        private void CreateSection()
        {
            GameObject sectionBackground = Instantiate(BGObject, parent.transform);
            sectionBackground.transform.position = new Vector3(locWidth, locHeight, -5f);
            sectionBackground.GetComponent<RectTransform>().sizeDelta = new Vector2(width, height);

            ChangeBGImage(sectionBackground);

            if (buttons.Length > 0)
                CreateSectorButtons();
        }

        private void ChangeBGImage(GameObject sectionBackground)
        {
            if (customBackground != null)
                sectionBackground.GetComponent<Image>().sprite = customBackground;
            else if (defaultBG == null)
                sectionBackground.GetComponent<Image>().enabled = false;
            else
                sectionBackground.GetComponent<Image>().sprite = defaultBG;
        }

        private void CreateSectorButtons()
        {
            for (int i = 0; i < buttons.Length; i++)
            {
                float buttonHeight = height * percentageOfSectorUsageOverall / buttons.Length;
                float buttonWidth;
                if (buttonWidthScale != 0)
                    buttonWidth = buttonHeight * buttonWidthScale;
                else
                    buttonWidth = width * percentageOfSectorUsageOverall;
                float buttonLocHeight = locHeight + height / 2 - ((height - height * percentageOfSectorUsageOverall) / 2) - (buttonHeight * i) - buttonHeight / 2;
                float buttonLocWidth = locWidth;
                buttons[i].CreateButton(buttonLocHeight, buttonLocWidth, buttonHeight, buttonWidth, parent, BGObject, buttonObject, defaultButtonBG);
            }
        }
    }

    [System.Serializable]
    public class ButtonSection
    {
        public bool flavor;
        public bool cupSize;
        public bool targetForIceCreamConstruction;
        public bool customerSpot;
        public Sprite customButtonBGElement;
        public GameObject menuToOpen;

        public void CreateButton(float recLocHeight, float recLocWidth, float recHeight, float recWidth, GameObject parent, GameObject recBGElement, GameObject recButton, Sprite recDefaultButtonBG)
        {
            GameObject buttonBG = Instantiate(recButton, parent.transform);
            buttonBG.transform.position = new Vector3(recLocWidth, recLocHeight, 2f);
            buttonBG.GetComponent<RectTransform>().sizeDelta = new Vector2(recWidth, recHeight);
            if (customButtonBGElement != null)
                buttonBG.GetComponent<Image>().sprite = customButtonBGElement;
            else
                buttonBG.GetComponent<Image>().sprite = recDefaultButtonBG;

            buttonBG.GetComponent<scr_ButtonScript>().buttonID = buttonBG.GetComponentInParent<scr_ButtonIDCalculator>().currentID;
            buttonBG.GetComponentInParent<scr_ButtonIDCalculator>().UpdateValues();
            if (flavor)
                buttonBG.GetComponent<scr_ButtonScript>().AssignFlavor();
            if (cupSize)
                buttonBG.GetComponent<scr_ButtonScript>().AssignCupSize();
            if (targetForIceCreamConstruction)
                buttonBG.GetComponent<scr_ButtonScript>().AssignAsSpriteTarget();
            if (customerSpot)
                buttonBG.GetComponentInParent<scr_CustomerHandler>().ReceiveCustomerCoordinates(buttonBG.transform.position, recWidth, recHeight);

            Color tempColor = buttonBG.GetComponent<Image>().color;
            buttonBG.GetComponent<Image>().color = new Color(tempColor.r, tempColor.g, tempColor.b, 0f);

            //if (menuToOpen != null)
            //    buttonTextElement.GetComponent<scr_ButtonPress>().menuOnClick = menuToOpen;
            //if (hoverGraphic)
            //{
            //    buttonTextElement.GetComponent<scr_ButtonPress>().hoverObject = buttonBG;
            //    buttonTextElement.GetComponent<scr_ButtonPress>().OnHoverExit();
            //}
        }
    }

}